import numpy as np


class SeqData:
    def __init__(self, description, seq):
        self.description = description
        self.seq = seq


# podaj gi or accesion
def getSeqDataFromNcbi(gi):
    from Bio import Entrez
    from Bio import SeqIO
    Entrez.email = "A.N.Other@example.com"
    with Entrez.efetch(
            db="nuccore", rettype="fasta", retmode="text", id=gi
    ) as handle:
        seq_record = SeqIO.read(handle, "fasta")
    seqData = SeqData(seq_record.description, seq_record.seq)
    return seqData


# 1393630358
# 1400687864
# print(getSeqDataFromNcbi("1393630358","nuccore").description)

# z konsoli
def getSeqDataFromInput(description, seq):
    return SeqData(description, seq)


# z pliku
def getSeqDataFromFile(filename):
    with open(filename, "r") as myfile:
        Lines = myfile.readlines()
        line = Lines[0]
    if line.startswith('>'):
        seqData = SeqData("", "")
        seqData.description = line[1:len(line)]
        seqData.seq = "".join(Lines[1:len(Lines)]).strip()
    return seqData



def getDotMatrix(seq1,seq2):
    if len(seq2)*len(seq1)>10000:
        seq1=seq1[0:100]
        seq2=seq2[0:100]
    rows =len(seq1)
    columns= len(seq2)
    dotMatrix = np.zeros([len(seq1), len(seq2)])
    for i in range(rows):
        for j in range(columns):
            dotMatrix[i][j] =(seq1[i]==seq2[j])
    return dotMatrix

def filtruj(dotMatrix,okno,prog):
    array=np.array(dotMatrix)
    window_size=okno
    treshold=prog
    rows =len(array)
    columns= len(array[0])
    filtr = np.zeros([rows, columns])
    for i in range(rows-window_size+1):
        for j in range(columns-window_size+1):
            window=array[i:(i+window_size),j:(j+window_size)]
            diagonal=np.array(window).diagonal()
            anti_diagonal=np.fliplr(window).diagonal()
            if sum(diagonal)>=(window_size-treshold):
                np.fill_diagonal(filtr[i:i+window_size,j:j+window_size],diagonal)
            if sum(anti_diagonal)>=(window_size-treshold):
                np.fill_diagonal(np.fliplr(filtr[i:i+window_size,j:j+window_size]),anti_diagonal)
    return filtr



    return przefiltrowana
def split(word):
    return [char for char in word]


def run():
    sposob = input("Podaj sposób wczytywania: 1 ręcznie, 2 z pliku, 3 z bazy ncbi\n")
    if sposob == "1":
        print("Podaj nazwę pierwszej sekwencji:\n")
        print("Podaj pierwszą sekwencje:\n")
        seq1Data = getSeqDataFromInput(input(), input())
        print("Podaj nazwę drugiej sekwencji:\n")
        print("Podaj drugą sekwencje:\n")
        seq2Data = getSeqDataFromInput(input(), input())
    elif sposob == "2":
        seq1Data = getSeqDataFromFile(input("Podaj nazwę pliku pierwszej sekwencji\n"))
        seq2Data = getSeqDataFromFile(input("Podaj nazwę pliku drugiej sekwencji\n"))
    elif sposob == "3":
        seq1Data = getSeqDataFromNcbi(input("Podaj GI lub ACCESION pierwszej sekwencji\n"))
        seq2Data = getSeqDataFromNcbi(input("Podaj GI lub ACCESION drugiej sekwencji\n"))
    okno = int(input("Podaj rozmiar okna:\n"))
    filtr = int(input("Podaj próg filtru:\n"))


    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(1,1,figsize=(10,10))


    yticks = split(seq1Data.seq)
    xticks = split(seq2Data.seq)
    F=getDotMatrix(seq1Data.seq, seq2Data.seq)
    if F.size<100:
        markersize=12
        plt.spy(F,markersize=markersize)
        plt.spy(filtruj(F,okno,filtr),markersize=markersize,mec='r')
        plt.xticks(np.arange(len(xticks)), xticks)
        plt.yticks(np.arange(len(yticks)), yticks)
        plt.xlabel(seq2Data.description, fontsize=14)
        plt.ylabel(seq1Data.description,fontsize=14)
    else:
        markersize=5
        plt.spy(F,markersize=markersize)
        plt.spy(filtruj(F,okno,filtr),markersize=markersize,mec='r')
        plt.xlabel(seq2Data.description, fontsize=10)
        plt.ylabel(seq1Data.description,fontsize=10)



    ax.xaxis.set_label_position('top')
    plt.ylabel(seq1Data.description)
    plt.title("Macierz kropkowa",pad=20)
    # plt.tight_layout()
    fig = ax.get_figure()
    fig.savefig(input("Podaj nazwę pliku do zapisania macierzy kropkowej:\n"))
    plt.show()




